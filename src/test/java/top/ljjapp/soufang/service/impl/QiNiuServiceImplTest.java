package top.ljjapp.soufang.service.impl;

import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import top.ljjapp.soufang.SoufangApplicationTests;
import top.ljjapp.soufang.service.IQiNiuService;

import java.io.File;

import static org.junit.Assert.*;

public class QiNiuServiceImplTest extends SoufangApplicationTests {

    @Autowired
    private IQiNiuService qiNiuService;

    @Test
    public void uploadFile() {
        String fileNme = "D:\\IdeaProjects\\soufang\\tmp\\该摸了.jpg";
        File file = new File(fileNme);

        try {
            Response response = qiNiuService.uploadFile(file);
            Assert.assertTrue(response.isOK());
        } catch (QiniuException e) {
            e.printStackTrace();
        }
    }
}