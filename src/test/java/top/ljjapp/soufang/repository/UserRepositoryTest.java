package top.ljjapp.soufang.repository;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import top.ljjapp.soufang.SoufangApplicationTests;
import top.ljjapp.soufang.entity.User;

import java.util.List;


public class UserRepositoryTest extends SoufangApplicationTests {

    @Autowired
    private UserRepository repository;

    @Test
    public void findOneTest(){
        List<User> all = repository.findAll();
        User one = repository.findOne(1L);
        System.out.println(one.toString());
        Assert.assertNotNull(one);
    }
}