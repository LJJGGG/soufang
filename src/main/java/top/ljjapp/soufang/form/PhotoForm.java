package top.ljjapp.soufang.form;

import lombok.Data;

@Data
public class PhotoForm {
    private String path;

    private int width;

    private int height;

}
