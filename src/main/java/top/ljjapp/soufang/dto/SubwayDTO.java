package top.ljjapp.soufang.dto;

import lombok.Data;

@Data
public class SubwayDTO {
    private Long id;
    private String name;
    private String cityEnName;

}
