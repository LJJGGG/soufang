package top.ljjapp.soufang.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import top.ljjapp.soufang.entity.HouseDetail;

import java.util.List;



public interface HouseDetailRepository extends JpaRepository<HouseDetail, Long> {
    HouseDetail findByHouseId(Long houseId);

    List<HouseDetail> findAllByHouseIdIn(List<Long> houseIds);
}
