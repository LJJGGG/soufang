package top.ljjapp.soufang.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import top.ljjapp.soufang.entity.Role;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role,Long> {
    List<Role> findRoleByUserId(Long userId);
}
