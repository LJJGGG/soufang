package top.ljjapp.soufang.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import top.ljjapp.soufang.entity.Subway;

import java.util.List;


public interface SubwayRepository extends JpaRepository<Subway, Long> {
    /**
     * 根据城市英文简写查找所有地铁线路
     * @param cityEnName
     * @return
     */
    List<Subway> findAllByCityEnName(String cityEnName);
}
