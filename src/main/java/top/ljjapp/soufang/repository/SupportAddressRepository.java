package top.ljjapp.soufang.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import top.ljjapp.soufang.entity.SupportAddress;

import java.util.List;

public interface SupportAddressRepository extends JpaRepository<SupportAddress,Long> {
    /**
     * 根据行政级别查询
     * @param level
     * @return
     */
    List<SupportAddress> findByLevel(String level);

    /**
     * 根据英文缩写和水平查询
     * @param enName
     * @param level
     * @return
     */
    SupportAddress findByEnNameAndLevel(String enName , String level);

    SupportAddress findByEnNameAndBelongTo(String enName, String belongTo);

    List<SupportAddress> findAllByLevelAndBelongTo(String level, String belongTo);
}
