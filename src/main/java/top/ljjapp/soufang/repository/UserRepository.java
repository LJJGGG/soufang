package top.ljjapp.soufang.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import top.ljjapp.soufang.entity.User;

public interface UserRepository extends JpaRepository<User,Long> {
    User findByName(String name);

    User findUserByPhoneNumber(String telephone);

    /**
     * 修改用户名
     * @param id
     * @param name
     */
    @Modifying
    @Query("update User as user set user.name = :name where id = :id")
    void updateUsername(@Param(value = "id")Long id, @Param(value = "name")String name);

    /**
     * 修改邮箱
     * @param id
     * @param email
     */
    @Modifying
    @Query("update User as user set user.email = :email where id = :id")
    void updateEmail(@Param(value = "id")Long id, @Param(value = "email")String email);

    /**
     * 修改Miami
     * @param id
     * @param password
     */
    @Modifying
    @Query("update User as user set user.password = :password where id = :id")
    void updatePassword(@Param(value = "id")Long id, @Param(value = "password")String password);
}
