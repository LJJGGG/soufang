package top.ljjapp.soufang.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import top.ljjapp.soufang.entity.HouseTag;

import java.util.List;


public interface HouseTagRepository extends JpaRepository<HouseTag, Long> {
    HouseTag findByNameAndHouseId(String name, Long houseId);

    /**
     * 根据id查询所有房源标签
     * @param id
     * @return
     */
    List<HouseTag> findAllByHouseId(Long id);

    List<HouseTag> findAllByHouseIdIn(List<Long> houseIds);
}
