package top.ljjapp.soufang.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import top.ljjapp.soufang.entity.HousePicture;

import java.util.List;


public interface HousePictureRepository extends JpaRepository<HousePicture, Long> {
    List<HousePicture> findAllByHouseId(Long id);
}
