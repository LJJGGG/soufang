package top.ljjapp.soufang.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import top.ljjapp.soufang.entity.SubwayStation;

import java.util.List;


public interface SubwayStationRepository extends JpaRepository<SubwayStation, Long> {
    /**
     * 根据subwayId查询所有地铁站
     * @param subwayId
     * @return
     */
    List<SubwayStation> findAllBySubwayId(Long subwayId);
}
