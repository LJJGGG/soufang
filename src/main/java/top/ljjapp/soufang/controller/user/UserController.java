package top.ljjapp.soufang.controller.user;

import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import top.ljjapp.soufang.base.ApiResponse;
import top.ljjapp.soufang.base.HouseSubscribeStatus;
import top.ljjapp.soufang.base.LoginUserUtil;
import top.ljjapp.soufang.dto.HouseDTO;
import top.ljjapp.soufang.dto.HouseSubscribeDTO;
import top.ljjapp.soufang.dto.UserDTO;
import top.ljjapp.soufang.entity.User;
import top.ljjapp.soufang.repository.UserRepository;
import top.ljjapp.soufang.service.IHouseService;
import top.ljjapp.soufang.service.IUserService;
import top.ljjapp.soufang.service.ServiceMultiResult;
import top.ljjapp.soufang.service.ServiceResult;

import java.util.Date;

@Controller
public class UserController {

    @Autowired
    private IUserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private IHouseService houseService;

    @GetMapping("/user/login")
    public String login(){
        return "user/login";
    }

    @GetMapping("/user/center")
    public String center(Model model) {
        //获取当前登录用户id
        Long loginUserId = LoginUserUtil.getLoginUserId();
        User user = userRepository.findOne(loginUserId);
        String email = user.getEmail();
        String name = user.getName();
        model.addAttribute("name", name);
        model.addAttribute("email", email);
        return "user/center";
    }

    /**
     * 修改用户信息
     * @param profile
     * @param value
     * @return
     */
    @PostMapping(value = "api/user/info")
    @ResponseBody
    public ApiResponse updateUserInfo(@RequestParam(value = "profile")String profile,
                                      @RequestParam(value = "value")String value){
        if (value.isEmpty()){
            return ApiResponse.ofStatus(ApiResponse.Status.BAD_REQUEST);
        }
        if ("email".equals(profile) && !LoginUserUtil.checkEmail(value)) {
            return ApiResponse.ofMessage(HttpStatus.SC_BAD_REQUEST, "不支持的邮箱格式");
        }
        ServiceResult result = userService.modifyUserProfile(profile, value);
        if (result.isSuccess()) {
            return ApiResponse.ofSuccess("");
        } else {
            return ApiResponse.ofMessage(HttpStatus.SC_BAD_REQUEST, result.getMessage());
        }
    }

    /**
     * 预约看房功能
     * @param houseId
     * @return
     */
    @PostMapping(value = "api/user/house/subscribe")
    @ResponseBody
    public ApiResponse subscribeHouse(@RequestParam(value = "house_id") Long houseId) {
        ServiceResult result = houseService.addSubscribeOrder(houseId);
        if (result.isSuccess()) {
            return ApiResponse.ofSuccess("");
        } else {
            return ApiResponse.ofMessage(HttpStatus.SC_BAD_REQUEST, result.getMessage());
        }
    }

    /**
     * 预约看房的展示（分页）
     * @param start
     * @param size
     * @param status
     * @return
     */
    @GetMapping(value = "api/user/house/subscribe/list")
    @ResponseBody
    public ApiResponse subscribeList(
            @RequestParam(value = "start", defaultValue = "0") int start,
            @RequestParam(value = "size", defaultValue = "3") int size,
            @RequestParam(value = "status") int status) {
        //获取预约的列表
        ServiceMultiResult<Pair<HouseDTO, HouseSubscribeDTO>> result = houseService.querySubscribeList(HouseSubscribeStatus.of(status), start, size);
        if (result.getResultSize() == 0) {
            return ApiResponse.ofSuccess(result.getResult());
        }
        ApiResponse response = ApiResponse.ofSuccess(result.getResult());
        response.setMore(result.getTotal() > (start + size));
        return response;
    }

    /**
     * 预约看房时间
     * @param houseId
     * @param orderTime
     * @param desc
     * @param telephone
     * @return
     */
    @PostMapping(value = "api/user/house/subscribe/date")
    @ResponseBody
    public ApiResponse subscribeDate(
            @RequestParam(value = "houseId") Long houseId,
            @RequestParam(value = "orderTime") @DateTimeFormat(pattern = "yyyy-MM-dd") Date orderTime,
            @RequestParam(value = "desc", required = false) String desc,
            @RequestParam(value = "telephone") String telephone
    ) {
        //检查是否填写时间
        if (orderTime == null) {
            return ApiResponse.ofMessage(HttpStatus.SC_BAD_REQUEST, "请选择预约时间");
        }
        //检查手机号
        if (!LoginUserUtil.checkTelephone(telephone)) {
            return ApiResponse.ofMessage(HttpStatus.SC_BAD_REQUEST, "手机格式不正确");
        }
        //预约看房时间
        ServiceResult serviceResult = houseService.subscribe(houseId, orderTime, telephone, desc);
        if (serviceResult.isSuccess()) {
            return ApiResponse.ofStatus(ApiResponse.Status.SUCCESS);
        } else {
            return ApiResponse.ofMessage(HttpStatus.SC_BAD_REQUEST, serviceResult.getMessage());
        }
    }

    /**
     * 取消预约
     * @param houseId
     * @return
     */
    @DeleteMapping(value = "api/user/house/subscribe")
    @ResponseBody
    public ApiResponse cancelSubscribe(@RequestParam(value = "houseId") Long houseId) {
        //取消预约
        ServiceResult serviceResult = houseService.cancelSubscribe(houseId);
        if (serviceResult.isSuccess()) {
            return ApiResponse.ofStatus(ApiResponse.Status.SUCCESS);
        } else {
            return ApiResponse.ofMessage(HttpStatus.SC_BAD_REQUEST, serviceResult.getMessage());
        }
    }
}
