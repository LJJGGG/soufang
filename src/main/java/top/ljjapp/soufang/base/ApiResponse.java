package top.ljjapp.soufang.base;

import lombok.Data;

@Data
public class ApiResponse {
    private int code;

    private String message;

    private Object data;

    private boolean more;


    public ApiResponse(int code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ApiResponse(){
        this.code = Status.SUCCESS.getCode();
        this.message = Status.SUCCESS.getMessage();
    }


    public static  ApiResponse ofMessage(int code ,String message){
        return new ApiResponse(code,message,null);
    }

    public static  ApiResponse ofSuccess(Object data){
        return new ApiResponse(Status.SUCCESS.getCode(),Status.SUCCESS.getMessage(),data);

    }
    public static  ApiResponse ofStatus(Status status){
        return new ApiResponse(status.getCode(),status.getMessage(),null);
    }




    public enum Status{

        SUCCESS(200,"成功"),
        BAD_REQUEST(400,"BadRequest"),
        INTERNAL_SERVER_ERROR(500,""),
        NOT_VALID_PARAM(40005,"Not valid Params"),
        NOT_SUPPORTED_OPERATION(40006,"Operation not supported"),
        NOT_LOGIN(50000,"Not Login"),
        NOT_FOUND(40400,"没有找到");

        private int code;

        private String message;

        Status(int code ,String message){
            this.code = code;
            this.message = message;
        }

        public int getCode() {
            return code;
        }


        public String getMessage() {
            return message;
        }

    }
}
