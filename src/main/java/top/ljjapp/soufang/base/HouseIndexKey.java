package top.ljjapp.soufang.base;

/**
 * 索引关键词统一定义
 */
public class HouseIndexKey {
    public static final String HOUSE_ID = "houseId";

    public static final String TITLE = "title";//标题

    public static final String PRICE = "price";//价格
    public static final String AREA = "area";//面积
    public static final String CREATE_TIME = "createTime";
    public static final String LAST_UPDATE_TIME = "lastUpdateTime";
    public static final String CITY_EN_NAME = "cityEnName";//城市简写
    public static final String REGION_EN_NAME = "regionEnName";//区域简写
    public static final String DIRECTION = "direction";
    public static final String DISTANCE_TO_SUBWAY = "distanceToSubway";
    public static final String STREET = "street";
    public static final String DISTRICT = "district";
    public static final String DESCRIPTION = "description";
    public static final String LAYOUT_DESC = "layoutDesc";
    public static final String TRAFFIC = "traffic";
    public static final String ROUND_SERVICE = "roundService";
    public static final String RENT_WAY = "rentWay";//合租或整租
    public static final String SUBWAY_LINE_NAME = "subwayLineName";
    public static final String SUBWAY_STATION_NAME = "subwayStationName";
    public static final String TAGS = "tags";//标签

    public static final String AGG_DISTRICT = "agg_district";
    public static final String AGG_REGION = "agg_region";
}
