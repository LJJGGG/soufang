package top.ljjapp.soufang.base;

import lombok.Data;

/**
 * 关键词
 */
@Data
public class HouseSuggest {
    private String input;
    private int weight = 10; // 默认权重
}
