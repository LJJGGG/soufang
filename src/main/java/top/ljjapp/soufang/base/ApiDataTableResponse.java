package top.ljjapp.soufang.base;

import lombok.Data;

/**
 * datatable的响应格式
 */
@Data
public class ApiDataTableResponse extends ApiResponse {
    private int draw;
    private long recordsTotal;
    private long recordsFiltered;

    public ApiDataTableResponse(int code, String message, Object data) {
        super(code, message, data);
    }

    public ApiDataTableResponse(ApiResponse.Status status){
        this(status.getCode(),status.getMessage(),null);
    }
}
