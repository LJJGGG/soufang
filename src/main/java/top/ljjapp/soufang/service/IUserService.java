package top.ljjapp.soufang.service;

import top.ljjapp.soufang.dto.UserDTO;
import top.ljjapp.soufang.entity.User;

public interface IUserService {
    User findUserByNmae(String name);

    ServiceResult<UserDTO> findById(Long userId);

    User findUserByTelephone(String telephone);

    User addUserByPhone(String telephone);

    /**
     * 修改用户属性值
     * @param profile
     * @param value
     * @return
     */
    ServiceResult modifyUserProfile(String profile, String value);
}
