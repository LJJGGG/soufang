package top.ljjapp.soufang.service.impl;

import com.google.common.collect.Lists;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import top.ljjapp.soufang.base.LoginUserUtil;
import top.ljjapp.soufang.dto.UserDTO;
import top.ljjapp.soufang.entity.Role;
import top.ljjapp.soufang.entity.User;
import top.ljjapp.soufang.repository.RoleRepository;
import top.ljjapp.soufang.repository.UserRepository;
import top.ljjapp.soufang.service.IUserService;
import top.ljjapp.soufang.service.ServiceResult;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private ModelMapper modelMapper;

    private final Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();

    @Override
    public User findUserByNmae(String name) {
        User user = userRepository.findByName(name);
        if (user == null){
            return null;
        }
        List<Role> roles = roleRepository.findRoleByUserId(user.getId());

        if (roles == null || roles.isEmpty()){
            throw new DisabledException("权限非法");
        }

        List<GrantedAuthority> authorities = new ArrayList<>();
        roles.forEach(
                role -> authorities.add( new SimpleGrantedAuthority("ROLE_"+ role.getName()) )
        );
        user.setAuthorityList(authorities);
        return user;
    }

    /**
     * 根据id查询userDTO
     * @param userId
     * @return
     */
    @Override
    public ServiceResult<UserDTO> findById(Long userId) {
        User user = userRepository.findOne(userId);
        if (user == null) {
            return ServiceResult.notFound();
        }
        UserDTO userDTO = modelMapper.map(user, UserDTO.class);
        return ServiceResult.of(userDTO);
    }

    /**
     * 根据手机查找用户
     * @param telephone
     * @return
     */
    @Override
    public User findUserByTelephone(String telephone) {
        User user = userRepository.findUserByPhoneNumber(telephone);
        if (user == null){
            return  null;
        }
        List<Role> roles = roleRepository.findRoleByUserId(user.getId());
        if (roles == null || roles.isEmpty()){
            throw  new DisabledException("权限非法");
        }

        List<GrantedAuthority> authorities = new ArrayList<>();
        roles.forEach(role -> authorities.add(new SimpleGrantedAuthority("ROLE_" +role.getName())));
        user.setAuthorityList(authorities);
        return user;
    }

    /**
     * 手机注册用户
     * @param telephone
     * @return
     */
    @Override
    @Transactional
    public User addUserByPhone(String telephone) {
        User user = new User();
        user.setPhoneNumber(telephone);
        user.setName(telephone.substring(0, 3) + "****" + telephone.substring(7, telephone.length()));
        Date now = new Date();
        user.setCreateTime(now);
        user.setLastLoginTime(now);
        user.setLastUpdateTime(now);
        user = userRepository.save(user);

        Role role = new Role();
        role.setName("USER");
        role.setUserId(user.getId());
        roleRepository.save(role);
        user.setAuthorityList(Lists.newArrayList(new SimpleGrantedAuthority("ROLE_USER")));
        return user;
    }

    /**
     * 修改用户属性值
     * @param profile
     * @param value
     * @return
     */
    @Override
    @Transactional
    public ServiceResult modifyUserProfile(String profile, String value) {
        //获取当前登录用户的id
        Long userId = LoginUserUtil.getLoginUserId();
        if (profile == null || profile.isEmpty()){
            return new ServiceResult(false, "修改值不能为空");
        }
        switch (profile){
            case "name":
                userRepository.updateUsername(userId, value);
                break;
            case "email":
                userRepository.updateEmail(userId, value);
                break;
            case "password":
                userRepository.updatePassword(userId, this.passwordEncoder.encodePassword(value, userId));
                break;
            default:
                return new ServiceResult(false, "该值修改不支持");
        }
        return ServiceResult.success();
    }
}
