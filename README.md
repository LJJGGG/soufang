# soufang
这是个基于springboot,Elasticsearch的javaweb项目

当然不是我自己写的（自己这么菜怎么写出来这些），跟着视频做的，主要目的是学习Elasticsearch在实际项目中的用处。

# 要启动项目需要配置什么

开发工具用的idea，使用了lombok插件

1,mysql
目前用我的阿里云上的，名字叫xunwu

2，redis 
目前用我的阿里云上的，一直启动着

3，七牛云
用来存储图片的，一个月换一次域名，需要accessKey,secreKey,Bucket(名称),域名前缀

4，kafka
~~消息队列，本机安装~~

放到云服务器上了

5，elasticsearch
ES，本机安装

6，阿里云短信
自己申请有免费的100条，我的没剩多少

7，email
监控ES健康状况，自己163上设置smtp并且设置上密码
